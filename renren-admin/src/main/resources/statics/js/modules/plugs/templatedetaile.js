$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'plugs/templatedetaile/list',
        datatype: "json",
        colModel: [			
			{ label: 'templateId', name: 'templateId', index: 'template_id', width: 50, key: true },
			{ label: 'key名称', name: 'name', index: 'name', width: 80 }, 			
			{ label: 'key值', name: 'val', index: 'val', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		templateDetaile: {},
        isSelect:false,
        templateList:{},
        q:{
            templateId: null
        },
	},
    beforeCreate: function(){
        $.get(baseURL + "plugs/template/listAll", function(r){
            vm.templateList=r.list;
        });
    },
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.templateDetaile = {};
		},
		update: function (event) {
			var templateId = getSelectedRow();
			if(templateId == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(templateId)
		},
		saveOrUpdate: function (event) {
            var templateId = $("#templateId").val();
            if(templateId==undefined||templateId==""){
                layer.alert("请选择模板");
                return ;
            }
            var name =  $("#setValue option:selected").text();
            if(name==undefined||name==""){
                layer.alert("请正确操作");
                return ;
            }
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.templateDetaile.templateId == null ? "plugs/templatedetaile/save" : "plugs/templatedetaile/update";
                var data = {
                    templateId:templateId,
                    name:name,
                    val:$("#Value").val()
                }
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var templateIds = getSelectedRows();
			if(templateIds == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "plugs/templatedetaile/delete",
                        contentType: "application/json",
                        data: JSON.stringify(templateIds),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(templateId){
			$.get(baseURL + "plugs/templatedetaile/info/"+templateId, function(r){
                vm.templateDetaile = r.templateDetaile;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData:{'templateId': vm.q.templateId},
                page:page
            }).trigger("reloadGrid");
		},
        getSelectData: function(key){
            $.get(baseURL + "sys/dict/listbyTemplateId?templateId="+$("#templateId option:selected").val(), function(r){
                console.info(r);
                if(r.list.length==0){
                    $("#two").html("");
                    $("#Value").val("");
                    layer.alert("当前模板没有可以添加的字典数据");
                    return;
                }
                var addhtml = "<div class=\"form-group\" id='two'>\n" +
                    "                        <div class=\"col-sm-2 control-label\">字段名称</div>\n" +
                    "                        <div class=\"col-sm-10\">\n" +
                    "                            <select class=\"form-control\" id=\"setValue\" onchange=\"inputData();\">";
                $(r.list).each(function(i,n) {
                    console.log(i+" = "+n);
                    if(i==0){
                        addhtml+="<option value="+n.code+" selected>"+n.value+"</option>";
                    }else {
                        addhtml+="<option value="+n.code+">"+n.value+"</option>"
                    }
                });
                addhtml +="</select> </div> </div>";
                if(vm.isSelect==false){
                    $("#first").after(addhtml);
                }else {
                    $("#two").html(addhtml);
                }
                vm.isSelect = true;
                $("#Value").val($("#setValue option:selected").val());
            });
	}}
});