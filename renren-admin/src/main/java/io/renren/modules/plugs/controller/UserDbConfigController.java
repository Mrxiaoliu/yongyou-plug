package io.renren.modules.plugs.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.plugs.dao.UserDbConfigDao;
import io.renren.modules.plugs.entity.UserDbConfigEntity;
import io.renren.modules.plugs.service.UserDbConfigService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 用户数据库配置
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
@RestController
@RequestMapping("plugs/userdbconfig")
public class UserDbConfigController {
    @Autowired
    private UserDbConfigService userDbConfigService;
    @Autowired
    private UserDbConfigDao userDbConfigDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("plugs:userdbconfig:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userDbConfigService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 查询所有
     */
    @RequestMapping("/listAll")
    public R list(){
        List<UserDbConfigEntity> userDbConfigEntities = userDbConfigDao.selectList(null);
        return R.ok().put("list", userDbConfigEntities);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("plugs:userdbconfig:info")
    public R info(@PathVariable("id") Long id){
        UserDbConfigEntity userDbConfig = userDbConfigService.getById(id);

        return R.ok().put("userDbConfig", userDbConfig);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("plugs:userdbconfig:save")
    public R save(@RequestBody UserDbConfigEntity userDbConfig){
        userDbConfigService.save(userDbConfig);

        return R.ok();
    }

    /**
     * 初始保存
     */
    @RequestMapping(value = "/firstSave",method = RequestMethod.POST)
    public R firstSave(UserDbConfigEntity userDbConfig){
        userDbConfigService.save(userDbConfig);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("plugs:userdbconfig:update")
    public R update(@RequestBody UserDbConfigEntity userDbConfig){
        ValidatorUtils.validateEntity(userDbConfig);
        userDbConfigService.updateById(userDbConfig);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("plugs:userdbconfig:delete")
    public R delete(@RequestBody Long[] ids){
        userDbConfigService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
