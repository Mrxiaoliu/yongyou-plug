package io.renren.modules.plugs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户数据库配置
 * 
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
@Data
@TableName("user_db_config")
public class UserDbConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键自增
	 */
	@TableId
	private Long id;
	/**
	 * 帐套名
	 */
	private String name;
	/**
	 * 数据库
	 */
	private String db;
	/**
	 * 用户名
	 */
	private String user;
	/**
	 * 密码
	 */
	private String password;

}
