package io.renren.modules.plugs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.plugs.entity.SupplierEntity;

import java.util.Map;

/**
 * 
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 20:09:36
 */
public interface SupplierService extends IService<SupplierEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

