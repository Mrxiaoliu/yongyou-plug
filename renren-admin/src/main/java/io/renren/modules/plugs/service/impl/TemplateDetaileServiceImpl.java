package io.renren.modules.plugs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.plugs.dao.TemplateDetaileDao;
import io.renren.modules.plugs.entity.TemplateDetaileEntity;
import io.renren.modules.plugs.service.TemplateDetaileService;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("templateDetaileService")
public class TemplateDetaileServiceImpl extends ServiceImpl<TemplateDetaileDao, TemplateDetaileEntity> implements TemplateDetaileService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String templateId = (String)params.get("templateId");
        IPage<TemplateDetaileEntity> page = this.page(
                new Query<TemplateDetaileEntity>().getPage(params),
                new QueryWrapper<TemplateDetaileEntity>().eq("template_id",templateId)
        );

        return new PageUtils(page);
    }

}
