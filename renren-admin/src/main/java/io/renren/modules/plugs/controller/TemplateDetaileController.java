package io.renren.modules.plugs.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.plugs.entity.TemplateDetaileEntity;
import io.renren.modules.plugs.service.TemplateDetaileService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 模板明细
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
@RestController
@RequestMapping("plugs/templatedetaile")
public class TemplateDetaileController {
    @Autowired
    private TemplateDetaileService templateDetaileService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("plugs:templatedetaile:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = templateDetaileService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{templateId}")
    @RequiresPermissions("plugs:templatedetaile:info")
    public R info(@PathVariable("templateId") Long templateId){
        TemplateDetaileEntity templateDetaile = templateDetaileService.getById(templateId);

        return R.ok().put("templateDetaile", templateDetaile);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("plugs:templatedetaile:save")
    public R save(@RequestBody TemplateDetaileEntity templateDetaile){
        templateDetaileService.save(templateDetaile);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("plugs:templatedetaile:update")
    public R update(@RequestBody TemplateDetaileEntity templateDetaile){
        ValidatorUtils.validateEntity(templateDetaile);
        templateDetaileService.updateById(templateDetaile);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("plugs:templatedetaile:delete")
    public R delete(@RequestBody Long[] templateIds){
        templateDetaileService.removeByIds(Arrays.asList(templateIds));

        return R.ok();
    }

}
