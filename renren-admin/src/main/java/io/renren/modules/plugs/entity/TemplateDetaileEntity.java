package io.renren.modules.plugs.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 模板明细
 * 
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
@Data
@TableName("template_detaile")
public class TemplateDetaileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键自增
	 */
	private Long templateId;
	/**
	 * key名称
	 */
	private String name;
	/**
	 * key值
	 */
	private String val;
//	/**
//	 * 创建时间
//	 */
//	private Date createDate;
//	/**
//	 * 更新时间
//	 */
//	private Date updateDate;

}
