package io.renren.modules.plugs.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.plugs.dao.TemplateDao;
import io.renren.modules.plugs.entity.TemplateEntity;
import io.renren.modules.plugs.service.TemplateService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 模板
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
@RestController
@RequestMapping("plugs/template")
public class TemplateController {
    @Autowired
    private TemplateService templateService;
    @Autowired
    private TemplateDao templateDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("plugs:template:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = templateService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 获取所有
     */
    @RequestMapping("/listAll")
    public R list(){
        List<TemplateEntity> list = templateDao.selectList(null);
        return R.ok().put("list", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("plugs:template:info")
    public R info(@PathVariable("id") Long id){
        TemplateEntity template = templateService.getById(id);

        return R.ok().put("template", template);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("plugs:template:save")
    public R save(@RequestBody TemplateEntity template){
        templateService.save(template);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("plugs:template:update")
    public R update(@RequestBody TemplateEntity template){
        ValidatorUtils.validateEntity(template);
        templateService.updateById(template);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("plugs:template:delete")
    public R delete(@RequestBody Long[] ids){
        templateService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
