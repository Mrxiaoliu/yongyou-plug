package io.renren.modules.plugs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 20:09:36
 */
@Data
@TableName("supplier")
public class SupplierEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 日期
	 */
	@TableId
	private String date;
	/**
	 * 借方（收到）
	 */
	private String debit;
	/**
	 * 贷方（支出）
	 */
	private String credit;
	/**
	 * 银行科目
	 */
	private String subject;
	/**
	 * 摘要
	 */
	private String remarks;
	/**
	 * 供应商名称
	 */
	private String supplierName;
	/**
	 * 确认人
	 */
	private String confirmer;
	/**
	 * 库区
	 */
	private String reservoirArea;
	/**
	 * 确认时间
	 */
	private String confirmTime;
	/**
	 * 
	 */
	private Long id;

}
