package io.renren.modules.plugs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.plugs.entity.CustomerEntity;

import java.util.Map;

/**
 * 
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 20:09:36
 */
public interface CustomerService extends IService<CustomerEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

