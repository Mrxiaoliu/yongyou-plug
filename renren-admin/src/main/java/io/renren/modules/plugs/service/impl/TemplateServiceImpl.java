package io.renren.modules.plugs.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.plugs.dao.TemplateDao;
import io.renren.modules.plugs.entity.TemplateEntity;
import io.renren.modules.plugs.service.TemplateService;


@Service("templateService")
public class TemplateServiceImpl extends ServiceImpl<TemplateDao, TemplateEntity> implements TemplateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TemplateEntity> page = this.page(
                new Query<TemplateEntity>().getPage(params),
                new QueryWrapper<TemplateEntity>()
        );

        return new PageUtils(page);
    }

}
