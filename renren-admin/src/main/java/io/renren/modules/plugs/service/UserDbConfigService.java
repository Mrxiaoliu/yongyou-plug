package io.renren.modules.plugs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.plugs.entity.UserDbConfigEntity;

import java.util.Map;

/**
 * 用户数据库配置
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
public interface UserDbConfigService extends IService<UserDbConfigEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

