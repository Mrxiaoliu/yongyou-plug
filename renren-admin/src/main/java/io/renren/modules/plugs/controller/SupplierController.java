package io.renren.modules.plugs.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.plugs.entity.SupplierEntity;
import io.renren.modules.plugs.service.SupplierService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 20:09:36
 */
@RestController
@RequestMapping("plugs/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("plugs:supplier:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = supplierService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{date}")
    @RequiresPermissions("plugs:supplier:info")
    public R info(@PathVariable("date") String date){
        SupplierEntity supplier = supplierService.getById(date);

        return R.ok().put("supplier", supplier);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("plugs:supplier:save")
    public R save(@RequestBody SupplierEntity supplier){
        supplierService.save(supplier);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("plugs:supplier:update")
    public R update(@RequestBody SupplierEntity supplier){
        ValidatorUtils.validateEntity(supplier);
        supplierService.updateById(supplier);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("plugs:supplier:delete")
    public R delete(@RequestBody String[] dates){
        supplierService.removeByIds(Arrays.asList(dates));

        return R.ok();
    }

}
