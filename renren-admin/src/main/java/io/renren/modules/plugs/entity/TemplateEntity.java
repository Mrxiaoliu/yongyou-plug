package io.renren.modules.plugs.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 模板
 * 
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
@Data
@TableName("template")
public class TemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键自增
	 */
	@TableId
	private Long id;
	/**
	 * 模板名称
	 */
	private String name;
	/**
	 * 模板类型
	 */
	private String type;

}
