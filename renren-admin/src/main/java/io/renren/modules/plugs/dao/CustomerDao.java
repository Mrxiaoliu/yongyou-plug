package io.renren.modules.plugs.dao;

import io.renren.modules.plugs.entity.CustomerEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 20:09:36
 */
@Mapper
public interface CustomerDao extends BaseMapper<CustomerEntity> {
	
}
