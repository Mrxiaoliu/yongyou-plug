package io.renren.modules.plugs.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.plugs.entity.CustomerEntity;
import io.renren.modules.plugs.service.CustomerService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Map;



/**
 * 
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 20:09:36
 */
@RestController
@RequestMapping("plugs/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("plugs:customer:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = customerService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 上传excel
     * @param file
     * @param id
     * @return
     */
    @RequestMapping("/upload")
    public R upload(MultipartFile file, String id){



        return R.ok();
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("plugs:customer:info")
    public R info(@PathVariable("id") Long id){
        CustomerEntity customer = customerService.getById(id);

        return R.ok().put("customer", customer);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("plugs:customer:save")
    public R save(@RequestBody CustomerEntity customer){
        customerService.save(customer);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("plugs:customer:update")
    public R update(@RequestBody CustomerEntity customer){
        ValidatorUtils.validateEntity(customer);
        customerService.updateById(customer);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("plugs:customer:delete")
    public R delete(@RequestBody Long[] ids){
        customerService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
