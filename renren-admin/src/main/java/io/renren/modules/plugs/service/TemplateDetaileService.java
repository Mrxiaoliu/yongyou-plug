package io.renren.modules.plugs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.plugs.entity.TemplateDetaileEntity;

import java.util.Map;

/**
 * 模板明细
 *
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
public interface TemplateDetaileService extends IService<TemplateDetaileEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

