package io.renren.modules.plugs.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.plugs.dao.UserDbConfigDao;
import io.renren.modules.plugs.entity.UserDbConfigEntity;
import io.renren.modules.plugs.service.UserDbConfigService;


@Service("userDbConfigService")
public class UserDbConfigServiceImpl extends ServiceImpl<UserDbConfigDao, UserDbConfigEntity> implements UserDbConfigService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserDbConfigEntity> page = this.page(
                new Query<UserDbConfigEntity>().getPage(params),
                new QueryWrapper<UserDbConfigEntity>()
        );

        return new PageUtils(page);
    }

}
