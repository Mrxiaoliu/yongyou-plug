package io.renren.modules.plugs.dao;

import io.renren.modules.plugs.entity.TemplateEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 模板
 * 
 * @author ljp
 * @email ${email}
 * @date 2020-11-05 22:01:30
 */
@Mapper
public interface TemplateDao extends BaseMapper<TemplateEntity> {
	
}
