/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.plugs.dao.TemplateDao;
import io.renren.modules.plugs.dao.TemplateDetaileDao;
import io.renren.modules.plugs.entity.TemplateDetaileEntity;
import io.renren.modules.plugs.entity.TemplateEntity;
import io.renren.modules.sys.dao.SysDictDao;
import io.renren.modules.sys.entity.SysDictEntity;
import io.renren.modules.sys.service.SysDictService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 数据字典
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("sys/dict")
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;
    @Autowired
    private SysDictDao sysDictDao;
    @Autowired
    private TemplateDao templateDao;
    @Autowired
    private TemplateDetaileDao templateDetaileDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:dict:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sysDictService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 通过type查询
     */
    @RequestMapping("/listbyTemplateId")
    public R list(@RequestParam Long templateId){
        TemplateEntity templateEntity = templateDao.selectById(templateId);
        QueryWrapper<TemplateDetaileEntity> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("template_id",templateId);
        List<TemplateDetaileEntity> templateDetaileEntities = templateDetaileDao.selectList(queryWrapper1);
        List<String> vals = new ArrayList<>();
        //查询所有
        QueryWrapper<SysDictEntity> queryWrapper2 = new QueryWrapper<SysDictEntity>();
        queryWrapper2.eq("type",templateEntity.getType());
        List<SysDictEntity> sysDictEntities = sysDictDao.selectList(queryWrapper2);
        List<SysDictEntity> sysDictEntitiesNEW = new ArrayList<>();
        for(SysDictEntity sysDictEntity:sysDictEntities ){
            for( TemplateDetaileEntity templateDetaileEntity :templateDetaileEntities){
                if(!sysDictEntity.getCode().equals(templateDetaileEntity.getVal())){
                    sysDictEntitiesNEW.add(sysDictEntity);
                }
            }
        }
        return R.ok().put("list", sysDictEntitiesNEW);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:dict:info")
    public R info(@PathVariable("id") Long id){
        SysDictEntity dict = sysDictService.getById(id);

        return R.ok().put("dict", dict);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:dict:save")
    public R save(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);

        sysDictService.save(dict);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:dict:update")
    public R update(@RequestBody SysDictEntity dict){
        //校验类型
        ValidatorUtils.validateEntity(dict);

        sysDictService.updateById(dict);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:dict:delete")
    public R delete(@RequestBody Long[] ids){
        sysDictService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
