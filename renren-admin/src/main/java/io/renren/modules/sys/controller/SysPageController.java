/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.controller;

import io.renren.modules.plugs.entity.UserDbConfigEntity;
import io.renren.modules.plugs.service.UserDbConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 系统页面视图
 *
 * @author Mark sunlightcs@gmail.com
 */
@Controller
public class SysPageController {
	@Autowired
	private UserDbConfigService userDbConfigService;
	
	@RequestMapping("modules/{module}/{url}.html")
	public String module(@PathVariable("module") String module, @PathVariable("url") String url){
		return "modules/" + module + "/" + url;
	}

	@RequestMapping(value = {"/", "index.html"})
	public String index(){
		return "index";
	}

	@RequestMapping("index1.html")
	public String index1(){
		return "index1";
	}

	@RequestMapping("login.html")
	public String login(){
		List<UserDbConfigEntity> list = userDbConfigService.list();
		if(list.size()==0){
			return "userdbconfig";
		}
		return "login";
	}
	@RequestMapping("userdbconfig.html")
	public String dbconfig(){
		List<UserDbConfigEntity> list = userDbConfigService.list();
		if(list.size()!=0){
			return "login";
		}
		return "userdbconfig";
	}

	@RequestMapping("main.html")
	public String main(){
		return "main";
	}

	@RequestMapping("404.html")
	public String notFound(){
		return "404";
	}

}
