$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'plugs/customer/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '日期', name: 'date', index: 'date', width: 80 }, 			
			{ label: '借方（收到）', name: 'debit', index: 'debit', width: 80 }, 			
			{ label: '贷方（支出）', name: 'credit', index: 'credit', width: 80 }, 			
			{ label: '银行科目', name: 'subject', index: 'subject', width: 80 }, 			
			{ label: '摘要', name: 'remarks', index: 'remarks', width: 80 }, 			
			{ label: '客户名称', name: 'customerName', index: 'customer_name', width: 80 }, 			
			{ label: '确认人', name: 'confirmer', index: 'confirmer', width: 80 }, 			
			{ label: '库区', name: 'reservoirArea', index: 'reservoir_area', width: 80 }, 			
			{ label: '确认时间', name: 'confirmTime', index: 'confirm_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
    $("#uploadFile").click(function(){
        var formData = new FormData();
        formData.append('file', $('#file')[0].files[0]);
        formData.append('id', $('#templateId option:selected').val());
        $.ajax({
            url: baseURL + "plugs/customer/upload",
            data: formData,
            type : "POST",
            // 告诉jQuery不要去处理发送的数据，用于对data参数进行序列化处理 这里必须false
            processData : false,
            // 告诉jQuery不要去设置Content-Type请求头
            contentType : false,
            success : function(json) {
                var page = $("#jqGrid").jqGrid('getGridParam','page');
                $("#jqGrid").jqGrid('setGridParam',{
                    page:page
                }).trigger("reloadGrid");
            },
            error : function(json) {
            }
        });
    });
}
);

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		customer: {},
        templateList:{}
	},
    beforeCreate: function(){
        $.get(baseURL + "plugs/template/listAll", function(r){
            vm.templateList=r.list;
        });
    },
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.customer = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.customer.id == null ? "plugs/customer/save" : "plugs/customer/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.customer),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "plugs/customer/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "plugs/customer/info/"+id, function(r){
                vm.customer = r.customer;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});