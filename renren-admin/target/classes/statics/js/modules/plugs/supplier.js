$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'plugs/supplier/list',
        datatype: "json",
        colModel: [			
			{ label: 'date', name: 'date', index: 'date', width: 50, key: true },
			{ label: '借方（收到）', name: 'debit', index: 'debit', width: 80 }, 			
			{ label: '贷方（支出）', name: 'credit', index: 'credit', width: 80 }, 			
			{ label: '银行科目', name: 'subject', index: 'subject', width: 80 }, 			
			{ label: '摘要', name: 'remarks', index: 'remarks', width: 80 }, 			
			{ label: '供应商名称', name: 'supplierName', index: 'supplier_name', width: 80 }, 			
			{ label: '确认人', name: 'confirmer', index: 'confirmer', width: 80 }, 			
			{ label: '库区', name: 'reservoirArea', index: 'reservoir_area', width: 80 }, 			
			{ label: '确认时间', name: 'confirmTime', index: 'confirm_time', width: 80 }, 			
			{ label: '', name: 'id', index: 'id', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		supplier: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.supplier = {};
		},
		update: function (event) {
			var date = getSelectedRow();
			if(date == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(date)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.supplier.date == null ? "plugs/supplier/save" : "plugs/supplier/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.supplier),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var dates = getSelectedRows();
			if(dates == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "plugs/supplier/delete",
                        contentType: "application/json",
                        data: JSON.stringify(dates),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(date){
			$.get(baseURL + "plugs/supplier/info/"+date, function(r){
                vm.supplier = r.supplier;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});